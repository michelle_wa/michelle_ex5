#include "Rectangle.h"
#include "Polygon.h"
#include "Point.h"


void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char WHITE[] = { 255, 255, 255 };
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), WHITE, 100.0f).display(disp);
}

void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0};
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), BLACK, 100.0f).display(disp);
}

myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const string& type, const string& name) :Polygon(type, name)
{
	_points.push_back(a);
	_points.push_back(Point(a.getX() + length, a.getY() + width));
	this->_p1 = a;
	this->_length = length;
	this->_width = width;
}


myShapes::Rectangle::~Rectangle() { }

double myShapes::Rectangle::getArea() const
{
	return this->_width * this->_length;
}

double myShapes::Rectangle::getPerimeter() const
{
	return  (2 *(this->_width + this->_length));
}

void myShapes::Rectangle::move(const Point & other)
{
	this->_p1 += other;
}

void myShapes::Rectangle::printDetails() const
{
	cout << "%s", this->getName();
	cout << "       ";
	cout << "%s", this->getType();
	cout << "    ";
	cout << this->getArea();
	cout << "     ";
	cout << this->getPerimeter();
}

