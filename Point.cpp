#include "Point.h"

Point::Point() { }

Point::~Point() { }

Point::Point(double x, double y)
{
	this->_x = x;
	this->_y = y;
}

Point::Point(const Point & other)
{
	this->_x = other.getX();
	this->_y = other.getY();
}

double Point::getX() const
{
	return this->_x;
}

double Point::getY() const
{
	return this->_y;
}


double Point::distance(const Point & other) const
{
	double D_x = 0;
	double D_y = 0;
	double x1 = this->_x;
	double y1 = this->_y;
	double x2 = other.getX();
	double y2 = other.getY();

	D_x = pow(x2 - x1, 2);
	D_y = pow(y2 - y1, 2);

	return sqrt(D_x + D_y);
}

Point Point::operator+(const Point & other) const
{
	double x1 = this->_x + other.getX();
	double y1 = this->_y + other.getY();
	Point newPoint = Point((x1), (y1));
	return newPoint;
}

Point & Point::operator+=(const Point & other)
{
	this->_x = this->_x + other.getX();
	this->_y = this->_y + other.getY();
	return *this;
}


