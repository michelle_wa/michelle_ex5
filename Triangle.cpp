#include "Triangle.h"
#include "Polygon.h"
#include "..\source\repos\ex5_michelle\ex5_michelle\Triangle.h"


void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), GREEN, 100.0f).display(disp);
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}

Triangle::Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name) :Polygon(type, name)
{
	this->_a = a;
	_points.push_back(a);
	this->_b = b;
	_points.push_back(b);
	this->_c = c;
	_points.push_back(c);
}

Triangle::~Triangle() { }

double Triangle::getArea()
{
	double area = 0;
	double ab = this->_points[0].distance(this->_points[1]); // a
	double bc = this->_points[1].distance(this->_points[2]); // b

	return (ab * bc) / 2;
}

double Triangle::getPerimeter() const
{
	double preimeter = this->_a.distance(this->_b) + this->_a.distance(this->_c) + this->_b.distance(this->_c);
	return preimeter;
}


void Triangle::move(const Point& other)
{
	this->_a += other;
	this->_b += other;
	this->_c += other;
}
