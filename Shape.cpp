#include "Shape.h"

Shape::Shape(const string& name, const string& type)
{
	this->_name = name;
	this->_type = type;
}

void Shape::printDetails() const
{
	cout << "%s      \n", this->getName();
	cout << "%s      \n", this->getType();
}

string Shape::getType() const
{
	return this->_type;
}

string Shape::getName() const
{
	return this->_name;
}

