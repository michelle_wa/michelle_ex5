#include <iostream>
#include "Menu.h"
#include "Circle.h"
#include "Rectangle.h"
#include "Triangle.h"
#include "Arrow.h"
#include "Polygon.h"

Menu::Menu()
{
	_board = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
	int option = 0;
	int shapeOption = 0;
	int x1 = 0;
	int y1 = 0;
	int x2 = 0;
	int y2 = 0;
	int x3 = 0;
	int y3 = 0;
	int radius = 0;
	int length = 0;
	int width = 0;
	string name;
	string type;

	cout << "Enter 0 to add a new shape." << endl;
	cout << "Enter 1 to modify or get information from a current shape." << endl;
	cout << "Enter 2 to delete all of the shapes." << endl;
	cout << "Enter 3 to exit." << endl;
	switch (option)
	{
		case 0:
		{
			cout << "Enter 0 to add a circle." << endl;
			cout << "Enter 1 to add an arrow" << endl;
			cout << "Enter 2 to add a triangle" << endl;
			cout << "Enter 3 to add a rectangle" << endl;
			cin >> shapeOption;

			switch (shapeOption)
			{
				case 0: //Circle
				{
					Circle* circle;
					type = "Circle";
					cout << "Enter X of point number 1: " << endl;
					cin >> x1;
					cout << "Enter Y of point number 1: " << endl;
					cin >> y1;
					cout << "Enter radius:" << endl;
					cin >> radius;
					cout << "Enter the name of the shape:" << endl;
					cin >> name;
					Point p1(x1, y1);
					circle = Circle::Circle(p1, radius, type, name);

					break;
				}
				case 1: //Arrow
				{
					Arrow* arrow;
					type = "Arrow";
					cout << "Enter X of point number 1: " << endl;
					cin >> x1;
					cout << "Enter Y of point number 1: " << endl;
					cin >> y1;
					cout << "Enter X of point number 2: " << endl;
					cin >> x2;
					cout << "Enter Y of point number 2: " << endl;
					cin >> y2;
					cout << "Enter the name of the shape:" << endl;
					cin >> name;
					Point p1(x1, y1);
					Point p2(x2, y2);
					Arrow::Arrow(p1, p2, type, name);

					break;
				}
				case 2: //Triangle
				{
					Triangle* triangle;
					type = "Triangle";
					cout << "Enter X of point number 1: " << endl;
					cin >> x1;
					cout << "Enter Y of point number 1: " << endl;
					cin >> y1;

					cout << "Enter X of point number 2: " << endl;
					cin >> x2;
					cout << "Enter Y of point number 2: " << endl;
					cin >> y2;

					cout << "Enter X of point number 3: " << endl;
					cin >> x3;
					cout << "Enter Y of point number 3: " << endl;
					cin >> y3;
					cout << "Enter the name of the shape:" << endl;
					cin >> name;

					if ((x1 == x2 && x1 == x3 || x2 == x3 && x2 == x1 || x3 == x1 && x3 == x2) || (y1 == y2 && y1 == y3 || y2 == y3 && y2 == y1 || y3 == y1 && y3 == y2))
					{
						cout << "The points entered create a line." << endl;
					}
					Point p1(x1, y1);
					Point p2(x2, y2);
					Point p3(x3, y3);
					Triangle::Triangle(p1, p2, p3, type, name);

					break;
				} 
				case 3: //Rectangle
				{
					Rectangle* rectangle;
					type = "Rectangle";
					cout << "Enter the X of the top left corner: " << endl;
					cin >> x1;
					cout << "Enter the Y of the top left corner: " << endl;
					cin >> y1;
					cout << "Enter the length of the shape: " << endl;
					cin >> length;
					cout << "Enter the width of the shape: " << endl;
					cin >> width;
					cout << "Enter the name of the shape:" << endl;
					cin >> name;
					Point p1(x1, y1);
		
					if (length <= 0 || width <= 0)
					{
						cout << "Length or Width can't be 0. " << endl;
					}
					Rectangle::Rectangle(p1, length, width, type, name);

					break;
				}
				default:
				{
					break;
				}
			} 
		}
		case 1:
		{
			break;
		}

	} 
}


Menu::~Menu()
{
	_disp->close();
	delete _board;
	delete _disp;
}


